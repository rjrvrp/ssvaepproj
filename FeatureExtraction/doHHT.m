disp('in HHT');
% sec harmonics ? for both aud and vid
wholeSet = {[]; 'vid'; 'aud'};
det = {[];7.5;10;37;43};
for j = 1:4
    typ = type{1,j};
    dat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha);
    sum10 = 0;
    sum07 = 0;
    for c = 5:7
        acton = dat(:,c);
        imfs = eemd(acton,0,1);
        for imfindex = 2:size(imfs,2)
            try
                interdata = ifndq(imfs(:,imfindex),1/Fs)/(2*pi);
                if (max(interdata) >= 10.1)
                    sum10 = sum((interdata < 10.1) & (interdata > 9.9)) + sum10;
                    sum07 = sum((interdata < 7.6) & (interdata > 7.4)) + sum07;
                end
            catch
                continue
            end
        end
    end
    sum37 = 0;
    sum43 = 0;
    for c = 1:4
        acton = dat(:,c);
        imfs = eemd(acton,0,1);
        for imfindex = 2:size(imfs,2)
            try
                interdata = ifndq(imfs(:,imfindex),1/Fs)/(2*pi);
                if (max(interdata) >= 43.5)
                    sum37 = sum((interdata < 37.5) & (interdata > 36.5)) + sum37;
                    sum43 = sum((interdata < 43.5) & (interdata > 42.5)) + sum43;
                end
            catch
                continue
            end
        end
    end
    pres = ismember(cFre, cell2mat(type{2,j}));
    vid = sum07 > sum10;
    aud = sum37 > sum43;
    if(pres(2))
        vid = 1-vid;
    end
    if(pres(4))
        aud = 1-aud;
    end
    wholeSet = [wholeSet [type{1,j}; num2cell([vid;aud])] ];
    det = [det [type{1,j}; num2cell([sum07;sum10;sum37;sum43])] ];
end

methstr.(strcat(patie{n},nume{n},'summary','_',num2str(sorw{n}))) = wholeSet;
methstr.(strcat(patie{n},nume{n},'data','_',num2str(sorw{n}))) = det;