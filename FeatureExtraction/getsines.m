time = 0:1/Fs:16;
time = time(1:end-1);
f = flickering;
for k = 1:16
    all(1,:) = [0 f];
    for j=1:10
        dat = Data.EEG(:,j);
        dat = dat(1:k*512);
        timenew = time(1:k*512);
        tarcurr = target(j);
        for i=1:length(f)
            sinecosine{i} = [
                sin(2*pi*f(i)*timenew);
                cos(2*pi*f(i)*timenew);
                sin(4*pi*f(i)*timenew);
                cos(4*pi*f(i)*timenew);
                sin(6*pi*f(i)*timenew);
                cos(6*pi*f(i)*timenew)];
            [Wx{i}, Wy{i}, r(i), U{i}, V{i}, stats{i}] = canoncorr(dat,sinecosine{i}');
        end
        all(j+1,:) = [tarcurr r];
    end
    slice{k} = all;
%     slice{k}
end

% 30 - 70 cut off for consecutive
% start on 3 s data 
% at 5 s - recentmost is decided

% multichannel???????????

clear i