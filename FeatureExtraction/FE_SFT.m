function [phis, freq] = FE_SFT(varargin)

%FE_SFT Feature Extraction - Spectral F-Test
%
%[phis, freq] = FE_SFT()
%   - defaults to simple test case (noise added sine wave with amplitude 3
%   and 10 Hz frequency)
%
%[phis, freq] = FE_SFT(eegdata, Fs, n, L, f0)
%   - eegdata = (Fs X N) * chan matrix , chan = No of channels, Fs =
%   Sampling frequency, N = No of time samples for which the data is
%   presented, n = epoch length for FFT (No of seconds to which the fft
%   must be applied, it should be a factor of N, it needs to give the
%   required frequency by the frequency resoltuion obtained, if not it will
%   raise error. Can be set to [] - in this case, it will do the FFT on the
%   whole data, i.e., n = N), L = No of neighbouring frequencies to
%   consider while calculating the spectral F-test value, f0 = array of
%   frequencies at which the spectral F-test values amplitudes are required
%   - phase locked implementation and uses FE_FFT to do the FFT
%   - freq = the frequencies for which the SFT values are computed
%   (generally the same as the f0 array), phis = the SFT values of the
%   frequencies mentioned in freq for no of epochs generated for each
%   channel

switch nargin
    case 0
        [phis, freq] = FE_SFT_test();
        return
    case 5
    otherwise
        disp('Illegal number of arguments')
        return
end

[freqSpec, freq] = FE_FFT(varargin{1},varargin{2},varargin{3},[]);
f0 = varargin{5};
[pres, ind] = ismember(f0,freq);
if(length(find(pres)) ~= length(pres))
    disp('Error : freq not found\nTerminating func improperly')
    return
end
L = varargin{4};
yf02 = freqSpec(ind,:,:) .^ 2;
yfi2 = zeros(size(yf02));
for j = 1:size(yfi2,3)
    for i = 1:length(ind)
        newind = [(ind(i)-L/2 : ind(i)-1) (ind(i)+1 : ind(i)+L/2)];
        yfi2(i,:,j) = sum(freqSpec(newind,:,j) .^ 2) / L;
    end
end
for i=1:size(freqSpec,3)
    out(:,:,i) = yf02(:,:,i)./yfi2(:,:,i);
end
phis = out;
freq = f0';

end

function [phis, freq] = FE_SFT_test()

Fs = 256;
L = 6*Fs;
time = (0:L-1)' / Fs;
x = 3*sin(2*pi*10*time)  + rand(size(time));
[phis, freq] = FE_SFT(x,Fs,2,6,[9 10 11]);

end