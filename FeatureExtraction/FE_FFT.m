function [freqSpec, freq] = FE_FFT(varargin)

%FE_FFT Feature Extraction - Fast Fourier Transform
%
%[freqSpec, freq] = FE_FFT()
%   - defaults to simple test case (noise added sine wave with amplitude 3
%   and 10 Hz frequency)
%
%[freqSpec, freq] = FE_FFT(eegdata, Fs, n, f0)
%   - eegdata = (Fs X N) * chan matrix , chan = No of channels, Fs =
%   Sampling frequency, N = No of time samples for which the data is
%   presented, n = epoch length for FFT (No of seconds to which the fft
%   must be applied, it should be a factor of N, it needs to give the
%   required frequency by the frequency resoltuion obtained, if not it will
%   raise error. Can be set to [] - in this case, it will do the FFT on the
%   whole data, i.e., n = N), f0 = array of frequencies at which the
%   amplitudes are required (Can be set to [] - in this case, it will give
%   the amplitudes of all the possible frequencies from 0 with the
%   frequency resolution)
%   - flat-top-windows the data with 84% overlap (required for flat top
%   windows) for each channel and then computes FFT
%   - freq = the frequencies for which the FFT is done, freqSpec = the
%   amplitudes of the frequencies mentioned in freq for no of epochs
%   generated for each channel

switch nargin
    case 0
        [freqSpec, freq] = FE_FFT_test();
        return
    case 4
    otherwise
        disp('Illegal number of arguments')
        return
end

sz = size(varargin{1});
if (~isempty(varargin{3}))
    L = varargin{2} * varargin{3};
else
    L = sz(1);
end
eegdataWindowed = varargin{1} .* repmat(flattopwin(L),sz(1)/L,sz(2));
%     eegdata = reshape(varargin{1}, L, sz(1)/L, sz(2));
%     eegdata = eegdata .* repmat(flattopwin(L),1,sz(1)/L, sz(2));

noverlap = ceil(.84*L); % 84% overlap
apd = L - noverlap;
n = ceil(((sz(1) - L) / apd) + 1);
eegdataWindowed = [eegdataWindowed; zeros((n-1)*apd+L,sz(2))];

for i=1:size(eegdataWindowed,2) % applying overlap here 84% for flattopwin
    for j=1:n % no of frames found by AP
        ind = 1 + (j-1) * apd;
        eegdata(:,j,i) = eegdataWindowed(ind : ind+L-1,i);
    end
end
fftdata = fft(eegdata);
P2 = abs(fftdata/L);
P1 = P2(1:L/2+1,:,:);
P1(2:end-1,:,:) = 2*P1(2:end-1,:,:);
f = varargin{2}*(0:(L/2))'/L;
f0 = varargin{4}';
if (~isempty(f0))
    [pres, ind] = ismember(f0,f);
    if(length(find(pres)) ~= length(pres))
        disp('Error : freq not found')
        disp('Terminating func improperly')
        return
    end
    freqSpec = P1(ind,:,:);
    freq = f0;
else
    freqSpec = P1;
    freq = f;
end

end

function [freqSpec, freq] = FE_FFT_test()

Fs = 256;
L = 2*Fs;
time = (0:L-1)' / Fs;
x = 3 * sin(2 * pi * 10 * time) + rand(size(time));
[freqSpec, freq] = FE_FFT(x,Fs,[],[]);

% sumx = sum(x);
% NFFT = 2^nextpow2(L);

% 0.9375 hz - 1.9115 using NFFT ???
% 0.9523 hz - 1.921  without    sumx = 0.6180 and 0 hz - 0.014715

% L = Fs => 1 sec signal ? perfect accuracy ? (FE_FT(2,1,20,20))

end