function in = score(in)

cal = cell(7,1);
flds = fieldnames(in);
for i = 1:length(flds)
    indi = in.(flds{i});
    data = indi(:,2);
    check = indi(:,3);
    for j = 1:4
        dat = data{j};
        calv = dat(1,2) > dat(2,2);
        if (check{j} == 43)
            calv = ~calv;
        end
        if(calv)
            cal{j} = 1;
        else
            cal{j} = 0;
        end
    end
    for j = 5:7
        dat = data{j};
        calv = (dat(1,2)+dat(3,2)) > (dat(2,2)+dat(4,2));
        if (check{j} == 10)
            calv = ~calv;
        end
        if(calv)
            cal{j} = 1;
        else
            cal{j} = 0;
        end
    end
    in.(flds{i}) = [in.(flds{i}) cal];
end

end