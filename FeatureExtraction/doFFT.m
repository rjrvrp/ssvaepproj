disp('in FFT');
% sec harmo for aud?
% sess
lengthvs = [2 24 24 24 24 24 24];
subst = [4 0 0 0 0 0 0];
fftle{1} = []; fftle{2} = []; fftle{3} = 2; %fftle{4} = 3;
fftle{4} = 4; fftle{5} = 6; fftle{6} = 8; fftle{7} = 12;
session{1} = 'prev'; session{2} = 'full';

for sess = 3:7
    session{sess} = strcat('sec',num2str(fftle{sess}));
end

for sess = 1:length(session)
    for j = 1:4
        typ = type{1,j};
        ext = extra(j) -  subst(sess);
        first = fir(j) - subst(sess)/2;
        i = 1;
        for k = 1:7
            fr = fre{k};
            ch = cha(k);
            dat = S(first*Fs+1+ext:(first+lengthvs(sess))*Fs+ext,ch);
            summary{i,1} = ch;
            [freqspec, f] = FE_FFT(dat,256,fftle{sess},fr);
            summary{i,2} = [f sum(freqspec,2)];
            i = i + 1;
        end
        summ.(type{1,j}) = [summary type{2,j}];
    end
    if(~isfield(methstr,strcat(patie{n},nume{n},'_',num2str(sorw{n}))))
        methstr.(strcat(patie{n},nume{n},'_',num2str(sorw{n}))) = struct();
    end
    datameth = methstr.(strcat(patie{n},nume{n},'_',num2str(sorw{n})));
    
    if(sess ~= 1)
        summ = score(summ);
        summ.name = name{n};
        datameth.(strcat(patie{n},nume{n},session{sess},'_',num2str(sorw{n}))) = summ;
    else
        summ.name = name{n};
        datameth.(strcat(patie{n},nume{n},session{sess},'_',num2str(sorw{n}))) = summ;
    end
    methstr.(strcat(patie{n},nume{n},'_',num2str(sorw{n}))) = datameth;
    clear summ
end

compa = struct();
for i = 1:4
    full = datameth.(strcat(patie{n},nume{n},'full','_',num2str(sorw{n})));
    val = full.(type{1,i});
    new = val(:,1);
    dataaf = val(:,2);
    prev = datameth.(strcat(patie{n},nume{n},'prev','_',num2str(sorw{n})));
    databe = prev.(type{1,i});
    databe = databe(:,2);
    for j=1:7
        den = databe{j};
        frequ = den(:,1);
        bef = den(:,2);
        den = dataaf{j};
        aft = den(:,2);
        aft = aft ./ bef;
        hum{j} = [frequ aft];
    end
    new = [new hum' val(:,3)];
    compa.(type{1,i}) = new;
end
compa = score(compa);

fincol = cell(7*4,sess+2); % 5 needs to be managed
full = datameth.(strcat(patie{n},nume{n},'full','_',num2str(sorw{n})));
for i = 1:4
    val = compa.(type{1,i});
    val2 = full.(type{1,i});
    val3 = [];
    for sess = 3:7
        single = datameth.(strcat(patie{n},nume{n},session{sess},'_',num2str(sorw{n})));
        single = single.(type{1,i});
        val3 = [val3 single(:,4)];
    end
    fincol(7*i-6:7*i,:) = [val(:,[1 3 4]) val2(:,4) val3];
end
header = {'chan','freq','comp','direct'};
for sess = 3:7
    header = [header,session{sess}];
end
fincol = [ header ; fincol]; % if 5 gets managed, this wil change
methstr.(strcat(patie{n},nume{n},'comp','_',num2str(sorw{n}))) = fincol;

fftdatafe = {};

for i = 1:4
    val2 = full.(type{1,i});
    finvid = [];
    finaud = [];
    for j = 5:7
        videop = val2{j,2};
        videop = videop([1 2],2) + videop([3 4],2);
        finvid = [finvid; videop];
    end
    for j = 1:4
        audiop = val2{j,2};
        audiop = audiop(:,2);
        finaud = [finaud; audiop];
    end
    pres = ismember(cFre, cell2mat(type{2,i}));
    pfreq = cFre(pres);
    pfreq = pfreq(:);
    fftdatafe = [fftdatafe num2cell([finvid;finaud;pfreq])];
end
freqpoints = {7.5; 10; 7.5; 10; 7.5; 10; 37; 43; 37; 43; 37; 43; 37; 43; 'vidwin'; 'audwin'};
channels = {14; 14; 15; 15; 16; 16; 6; 6; 8; 8; 10; 10; 15; 15; []; []};

fftdatafe = [channels freqpoints fftdatafe];
fftdatafe = [{[],[],'LL','LR','RL','RR'}; fftdatafe];

methstr.(strcat(patie{n},nume{n},'data','_',num2str(sorw{n}))) = fftdatafe;