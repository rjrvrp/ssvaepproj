disp('in CCA');
time = 0:1/Fs:24;
time = time(1:end-1);
cFre = [7.5 10 37 43];
cuts{1} = 5:7;  cuts{2} = 1:4;

for i=1:4
    sinecos{i} = [
        sin(2*pi*cFre(i)*time);
        cos(2*pi*cFre(i)*time);
        sin(4*pi*cFre(i)*time);
        cos(4*pi*cFre(i)*time);
        sin(6*pi*cFre(i)*time);
        cos(6*pi*cFre(i)*time)];
end

clear newr
for j = 1:4
    dat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha);
    for i = 1:4
        [~, ~, r{j,i}] = canoncorr(dat(:,cuts{ceil(i/2)}),sinecos{i}');
        r{j,i} = sum(r{j,i});
    end
end

r = [num2cell(cFre) ; r];
r = [ [{[]}; type(1,:)'] r]';

oldr = r;

winn = [];
for j = 1:4
    pres = ismember(cFre, cell2mat(type{2,j}));
    pfreq = cFre(pres);
    pfreq = pfreq(:);
    winn = [winn pfreq];
end
r = [r; [{'vidwin';'audwin'} num2cell(winn) ]];
r = [{[];[];[];[];[];[];[]} r];

methstr.(strcat(patie{n},nume{n},'data','_',num2str(sorw{n}))) = r;

for i = 2:5
    for j = 2:5
        oldr{i,j} = sum(oldr{i,j});
    end
end

newr = cell2mat(oldr(2:end,2:end));
newr = [(newr(1,:) > newr(2,:)); newr(3,:) > newr(4,:)];

for j = 1:4
    pres = ismember(cFre, cell2mat(type{2,j}));
    if(pres(2))
        newr(1,j) = 1-newr(1,j);
    end
    if(pres(4))
        newr(2,j) = 1-newr(2,j);
    end
end

newr = uint8(newr);
newr = [ type(1,:); num2cell(newr)];
newr = [ {[],'vid','aud'}' newr];

methstr.(strcat(patie{n},nume{n},'sumry','_',num2str(sorw{n}))) = newr;
clear newr r
