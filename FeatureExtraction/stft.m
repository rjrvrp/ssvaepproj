dat = S(125*Fs+1+116:149*Fs+116,10);
[S1 ,F1, T1, P1] = spectrogram(dat,rectwin(2*Fs),Fs,2*Fs,Fs);
[S2 ,F2, T2, P2] = spectrogram(dat,flattopwin(2*Fs),431,2*Fs,Fs); % 84% overlap
freqs = [7.5 10 15 20 37 43];
[pres, ind] = ismember(freqs,F1);
p1part = P1(ind,:);
p2part = P2(ind,:);
yf02p1 = p1part .^ 2;
yf02p2 = p2part .^ 2;
yfi2p1 = zeros(length(ind),size(p1part,2));
yfi2p2 = zeros(length(ind),size(p2part,2));
L = 6;
for i = 1:length(ind)
    newind = [(ind(i)-L/2 : ind(i)-1) (ind(i)+1 : ind(i)+L/2)];
    yfi2p1(i,:) = sum(P1(newind,:) .^ 2) / L;
    yfi2p2(i,:) = sum(P2(newind,:) .^ 2) / L;
end
phip1 = [freqs' (yf02p1./yfi2p1)];
phip2 = [freqs' (yf02p2./yfi2p2)];