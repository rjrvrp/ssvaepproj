disp('in SFT');
epoch = [2 4 6 8 12 24];
maxL = 7.5 * epoch * 2;

crits = [ % L 0.05 0.10
    4   4.4590  3.11312;
    6   3.8853  2.8068;
    8   3.6337  2.66817;
    10  3.4928  2.58925;
    12  3.4028  2.53833;
    14  3.3404  2.50276;
%     15  3.3158  2.48872;
    20  3.2317  2.44037;
    30  3.1504  2.39325;
    60  3.0718  2.3734];

wholeSet = {'freq','epoch','L','0.05','0.10'};
% sec harmo for aud?
for eL = 1:length(epoch)
    for critIndex = 1:size(crits,1)
        if (crits(critIndex,1) > maxL(eL))
            continue
        end
        for j = 1:4
            typ = type{1,j};
            dat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha);
            [phis, freq] = FE_SFT(dat, Fs, epoch(eL), crits(critIndex,1), [7.5 10 15 20 37 43]);
            videop = [sum(sum(phis(1:4,:,5:7) > crits(critIndex,2),3),2) sum(sum(phis(1:4,:,5:7) > crits(critIndex,3),3),2)];
            audiop = [sum(sum(phis(5:6,:,1:4) > crits(critIndex,2),3),2) sum(sum(phis(5:6,:,1:4) > crits(critIndex,3),3),2)];
            videop = [videop(1,:)+videop(3,:); videop(2,:)+videop(4,:)];
            pres = ismember(cFre, cell2mat(type{2,j}));
            videop = videop(1,:) > videop(2,:);
            audiop = audiop(1,:) > audiop(2,:);
            if(pres(2))
                videop = 1-videop;
            end
            if(pres(4))
                audiop = 1-audiop;
            end
            wholeSet = [wholeSet; num2cell([cFre(pres)' [epoch(eL);epoch(eL)] [crits(critIndex,1);crits(critIndex,1)] [videop;audiop] ]) ];
        end
    end
end

eL = 6;
critIndex = 6;
sftdata = {};

freqpoints = {7.5; 10; 7.5; 10; 7.5; 10; 37; 43; 37; 43; 37; 43; 37; 43; 'vidwin'; 'audwin'};
channels = {14; 14; 15; 15; 16; 16; 6; 6; 8; 8; 10; 10; 15; 15; []; []};

for j = 1:4
    typ = type{1,j};
    dat = S(fir(j)*Fs+1+extra(j):(fir(j)+24)*Fs+extra(j),cha);
    [phis, freq] = FE_SFT(dat, Fs, epoch(eL), crits(critIndex,1), [7.5 10 15 20 37 43]);
    phis = squeeze(phis);
    videop = phis([1 2],5:7) + phis([3 4],5:7);
    videop = videop(:);
    audiop = phis(5:6,1:4);
    audiop = audiop(:);
    pres = ismember(cFre, cell2mat(type{2,j}));
    pfreq = cFre(pres);
    pfreq = pfreq(:);
%     if(pres(2))
%         videop = 1-videop;
%     end
%     if(pres(4))
%         audiop = 1-audiop;
%     end
    sftdata = [sftdata num2cell([videop;audiop;pfreq])];
end

sftdata = [channels freqpoints sftdata];
sftdata = [{[],[],'LL','LR','RL','RR'}; sftdata];

methstr.(strcat(patie{n},nume{n},'data','_',num2str(sorw{n}))) = sftdata;
methstr.(strcat(patie{n},nume{n},'summary','_',num2str(sorw{n}))) = wholeSet;