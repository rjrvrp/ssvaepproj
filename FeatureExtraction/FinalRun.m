clear all
home

fre{1} = [37 43];
fre{2} = [37 43];
fre{3} = [37 43];
fre{4} = [37 43];
fre{5} = [7.5 10 15 20];
fre{6} = [7.5 10 15 20];
fre{7} = [7.5 10 15 20];

cFre = [7.5 10 37 43];

addpath('C:\Users\USER\Documents\MATLAB\MajProj\Matlab runcode')

cha = [6 8 10 15 14 15 16];

type{1,1} = 'LL';
type{1,2} = 'LR';
type{1,3} = 'RL';
type{1,4} = 'RR';

methods{1} = 'FFT'; methods{2} = 'SFT'; methods{3} = 'CCA'; methods{4} = 'HHT';

Fs = 256;

% n
name{1} = '2016_03_03_14_07_05_Venkatesh_sess1';
starttime{1} = [18 53 89 125];
extrasamples{1} = [65   218    34    94];
sorw{1} = 0; % 0 = sine, 1 = words
patie{1} = 'ven';
nume{1} = '1';

name{2} = '2016_03_03_17_21_43_3rajwords';
starttime{2} = [18 53 89 125];
extrasamples{2} = [64   220    40   116];
sorw{2} = 1; % 0 = sine, 1 = words
patie{2} = 'raj';
nume{2} = '3';

name{3} = '2016_03_09_14_49_49_rupa1wordsproper';
starttime{3} = [18 53 89 125];
extrasamples{3} = [63   255    67   127];
sorw{3} = 1; % 0 = sine, 1 = words
patie{3} = 'rupa';
nume{3} = '1';

name{4} = '2016_03_09_14_53_39_rupa2words';
starttime{4} = [18 53 89 125];
extrasamples{4} = [64   208    20    77];
sorw{4} = 1; % 0 = sine, 1 = words
patie{4} = 'rupa';
nume{4} = '2';

name{5} = '2016_03_09_14_57_54_rupa3words';
starttime{5} = [18 53 89 125];
extrasamples{5} = [64   212    24    80]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{5} = 1; % 0 = sine, 1 = words
patie{5} = 'rupa';
nume{5} = '3';

name{6} = '2016_03_15_14_22_55_rama1wordsproper';
starttime{6} = [18 53 89 125];
extrasamples{6} = [63   216    32    92]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{6} = 1; % 0 = sine, 1 = words
patie{6} = 'rama';
nume{6} = '1';

name{7} = '2016_03_15_14_28_37_rama2words';
starttime{7} = [18 53 89 125];
extrasamples{7} = [62   214    26    86]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{7} = 1; % 0 = sine, 1 = words
patie{7} = 'rama';
nume{7} = '2';

name{8} = '2016_03_15_14_37_58_rama1sine';
starttime{8} = [18 53 89 125];
extrasamples{8} = [65   225    45   117]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{8} = 0; % 0 = sine, 1 = words
patie{8} = 'rama';
nume{8} = '1';

name{9} = '2016_03_15_14_47_38_rama2sineproper';
starttime{9} = [18 53 89 125];
extrasamples{9} = [ 65   217    29    86]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{9} = 0; % 0 = sine, 1 = words
patie{9} = 'rama';
nume{9} = '2';

name{10} = '2016_03_15_14_51_38_rama3words';
starttime{10} = [18 53 89 125];
extrasamples{10} = [65   213    33    93]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{10} = 1; % 0 = sine, 1 = words
patie{10} = 'rama';
nume{10} = '3';

name{11} = '2016_03_15_14_55_56_rama3sine';
starttime{11} = [18 53 89 125];
extrasamples{11} = [64   216    32    92]; % round([.246 .8242 .0898 .3085]*256)+1
sorw{11} = 0; % 0 = sine, 1 = words
patie{11} = 'rama';
nume{11} = '3';

warning off;
% load('AllDataCompre.mat');
analyData = struct();

for n = 1:11
    disp(n);
    HDR = sopen(strcat('C:\Users\USER\Desktop\Everything related to Major\MAJOR PROJECT\DATA\wetransfer-e920e1\',name{n},'.edf'), 'r');
    [S,HDR] = sread(HDR);
    HDR = sclose(HDR);
    
    fir = starttime{n};
    extra = extrasamples{n};
    if(sorw{n} == 0)
        type{2,1} = {37;37;37;37;7.5;7.5;7.5};
        type{2,2} = {43;43;43;43;7.5;7.5;7.5};
        type{2,3} = {37;37;37;37;10;10;10};
        type{2,4} = {43;43;43;43;10;10;10};
    else
        type{2,1} = {43;43;43;43;7.5;7.5;7.5};
        type{2,2} = {37;37;37;37;7.5;7.5;7.5};
        type{2,3} = {43;43;43;43;10;10;10};
        type{2,4} = {37;37;37;37;10;10;10};
    end
    
    if (~isfield(analyData,patie{n}))
        analyData.(patie{n}) = struct();
    end
    subj = analyData.(patie{n});
    
    for methNo = 1:3
        
        method = methods{methNo};
        if(~isfield(subj,method))
            subj.(method) = struct();
        end
        methstr = subj.(method);
        run(strcat('do',method));
        subj.(method) = methstr;
        
    end
    
    analyData.(patie{n}) = subj;
    
end

save('AllDataCompre','analyData','nume','sorw','patie','methods');

clear HDR

% compare each part of the 2/3 sec fft with the prev and find the peak
% do for many  msecsond and continue 