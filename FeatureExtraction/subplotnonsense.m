R = 6;
C = 4;
figure;
hold on;
for i=1:R
    for j = 1:C
        subplot(R,C,(i-1)*C+j)
        plot(freqs(1:4),phip2(1:4,(i-1)*C+j+1) ... 
            ,'*')
%         ylim([0 2]);
%         xlim([0,10])
        title((i-1)*C+j);
    end
end
clear i j

% for i=1:14
%     plot(1:15360,imfsnew(:,i)-(5e-5)*(i-1))
% end