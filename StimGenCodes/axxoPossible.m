% ALL the possible Sets

%%
T = 24;
Fs = 44100;                    % sampling rate for the modulating signal 
t = 0:1/Fs:T;
t = t(1:end-1);

Fm = 37;                       % frequency of the modulating signal 
Fc = 2500;
modulating = sin(2*pi*Fm*t);
left = ammod(modulating,Fc,Fs)';

Fm = 43;                       % frequency of the modulating signal 
Fc = 1000;
modulating = sin(2*pi*Fm*t);
right = ammod(modulating,Fc,Fs)';

finalmat = [left right];
audiowrite('serialAudio_sine.wav',finalmat,Fs);

for i = 0:3
    j = 6*i + 3;
    finalmat(j*Fs+1:(3+j)*Fs,1) = zeros(3*Fs,1);
    j = 6*i;
    finalmat(j*Fs+1:(3+j)*Fs,2) = zeros(3*Fs,1);
end

audiowrite('dichoticAudio_sine.wav',finalmat,Fs);

%%
% 
% [y{1}, fsl] = audioread('right_proper_lauren_us.MP3');
% Ltimeend = length(y{1})/fsl;
% [y{2}, fsr] = audioread('left_proper_julia_us.MP3');
% Rtimeend = length(y{2})/fsr;
% [timeend, ind] = max([Ltimeend Rtimeend]);
% y{ ~(ind-1) + 1 } = [y{ ~(ind-1) + 1 }; 
%     zeros( length(y{ind}) - length(y{ ~(ind-1) + 1 }) ,1)];
% 
% h = y{1};
% j = y{2};
% 
% % for i=1:13
% %     y{1} = [y{1};h];
% %     y{2} = [y{2};j];
% % end
% 
% y{1} = [h;h;h;h;h;h;h;h;h;h;h;h;h;h;h];
% y{2} = [j;j;j;j;j;j;j;j;j;j;j;j;j;j;j];
% 
% val = 384000-length(y{1});
% 
% y{1} = [y{1}; h(1:val)];
% y{2} = [y{2}; h(1:val)];
% 
% FmL = 37;
% FmR = 43;
% t = 0:1/fsl:24;
% t = t(1:end-1);
% modulating{1} = sin(2*pi*FmL*t);
% modulating{2} = sin(2*pi*FmR*t);
% 
% left = modulating{1}' .* y{1};
% right = modulating{2}' .* y{2};
% 
% rat_2 = ((left' * left) / (right' * right)) ^ 0.5;
% finalmat = [rat_2*right left];
% % audiowrite('serialAudio_words.wav',finalmat,fsr);
% 
% 
% for i = 0:3
%     j = 6*i + 3;
%     finalmat(j*fsl+1:(3+j)*fsl,1) = zeros(3*fsl,1);
%     j = 6*i;
%     finalmat(j*fsl+1:(3+j)*fsl,2) = zeros(3*fsl,1);
% end
% 
% audiowrite('dichoticAudio_words.wav',finalmat,fsr);


%%
% finalmat = [finalmat; zeros(480,2)];
% videoFReader = vision.VideoFileReader('serialVideo.wmv');
% videoFWriter = vision.VideoFileWriter('combBinaurWords.wmv', ... 
%     'FrameRate', videoFReader.info.VideoFrameRate, ... 
%     'FileFormat', 'WMV', ... 
%     'AudioInputPort', true);
% j = 1;
% for i=1:24*60
%     if(rem(i,60) == 0)
%         disp(i/60);
%     end
%     videoFrame = step(videoFReader);
%     step(videoFWriter, videoFrame, finalmat(j:i*267,:));
%     j = j + 267;
% end
% release(videoFReader); % close the input file
% release(videoFWriter); % close the output file
