function models = training(traindata,labels)

%stru = struct();
% stru.knn = fitcknn(traindata,labels,'NumNeighbors',5,'NSMethod','exhaustive','Distance','minkowski','BreakTies','nearest','BucketSize',100,'DistanceWeight','inverse');
% models = stru;
models{1,1} = 'knn';
models{1,2} = fitcknn(traindata,labels,'NumNeighbors',5,'NSMethod','exhaustive','Distance','minkowski','BreakTies','nearest','BucketSize',100,'DistanceWeight','inverse');
models{2,1} = 'dtree';
models{2,2} = fitctree(traindata,labels);
models{3,1} = 'lda';
models{3,2} = fitcdiscr(traindata,labels,'discrimType','pseudoLinear');
models{4,1} = 'qda';
models{4,2} = fitcdiscr(traindata,labels,'discrimType','quadratic');
models{5,1} = 'randomforest'
models{5,2} = TreeBagger(50,traindata,labels,'NVarToSample',2);

end