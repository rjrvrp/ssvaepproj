warning off;
global modelfuncs modelNo order

%% Model Data

modelfuncs{1} = @(traindata,labels) fitcknn(traindata,labels,'NumNeighbors',5,'NSMethod','exhaustive','Distance','minkowski','BreakTies','nearest','BucketSize',100,'DistanceWeight','inverse');
modelfuncs{2} = @(traindata,labels) fitctree(traindata,labels);
modelfuncs{3} = @(traindata,labels) fitcdiscr(traindata,labels,'discrimType','pseudoLinear');
modelfuncs{4} = @(traindata,labels) fitcdiscr(traindata,labels,'discrimType','quadratic');
modelfuncs{5} = @(traindata,labels) fitensemble(traindata,labels,'AdaBoostM1',200,'Discriminant');

%% FE Data

fem = {'FFT', 'SFT', 'CCA'};
vidDataMod = {1:6, 1:6, 1:2};
vidLabelsMod = {15, 15, 5};
audDataMod = {7:14, 7:14, 3:4};
audLabelsMod = {16, 16, 6};

%% Main Loop

finalAnalysis = cell(length(fem)*length(modelfuncs)*2,3);
index = 1;

conjoinVD = [];
conjoinVL = [];
conjoinAD = [];
conjoinAL = [];

for methNo = 1:length(fem)
    
    currMeth = fem{methNo};
    vidData = vidDataMod{methNo};
    vidLabels = vidLabelsMod{methNo};
    audData = audDataMod{methNo};
    audLabels = audLabelsMod{methNo};
    
    data = allFeatures.(currMeth).DATA;
    data = reshape(data,audLabels,[])';
    vidy = data(:,vidLabels);
    vidX = data(:,vidData);
    audy = data(:,audLabels);
    audX = data(:,audData);
    
    conjoinVD = [conjoinVD vidX];
    conjoinVL = vidy;
    conjoinAD = [conjoinAD audX];
    conjoinAL = audy;
    
    for modelNo = 1:length(modelfuncs)
        order = unique(vidy); % Order of the group labels
        cp = cvpartition(vidy,'k',10); % Stratified cross-validation
        cfMatVid = crossval(@crossValfunc,vidX,vidy,'partition',cp);
        cfMatVid = reshape(sum(cfMatVid),2,2);
        
        order = unique(audy); % Order of the group labels
        cp = cvpartition(audy,'k',10); % Stratified cross-validation
        cfMatAud = crossval(@crossValfunc,audX,audy,'partition',cp);
        cfMatAud = reshape(sum(cfMatAud),2,2);
        
        finalAnalysis{index,2} = cfMatVid;
        sum_total = sum(sum(cfMatVid));
        sum_diagonal = trace(cfMatVid);
        accuracy = ((sum_diagonal)/(sum_total)) * 100;
        finalAnalysis{index,3} = accuracy;
        finalAnalysis{index,1} = strcat(currMeth,',',num2str(modelNo),',Video');
        finalAnalysis{index+1,2} = cfMatAud;
        finalAnalysis{index+1,1} = strcat(currMeth,',',num2str(modelNo),',Audio');
        sum_total = sum(sum(cfMatAud));
        sum_diagonal = trace(cfMatAud);
        accuracy = ((sum_diagonal)/(sum_total)) * 100;
        finalAnalysis{index+1,3} = accuracy;
        index = index + 2;
        
    end
    
end

%% All FEs

for modelNo = 1:length(modelfuncs)
   
    order = unique(conjoinVL); % Order of the group labels
    cp = cvpartition(conjoinVL,'k',10); % Stratified cross-validation
    cfMatVid = crossval(@crossValfunc,conjoinVD,conjoinVL,'partition',cp);
    cfMatVid = reshape(sum(cfMatVid),2,2);
    
    order = unique(conjoinAL); % Order of the group labels
    cp = cvpartition(conjoinAL,'k',10); % Stratified cross-validation
    cfMatAud = crossval(@crossValfunc,conjoinAD,conjoinAL,'partition',cp);
    cfMatAud = reshape(sum(cfMatAud),2,2);
    
    finalAnalysis{index,2} = cfMatVid;
    sum_total = sum(sum(cfMatVid));
    sum_diagonal = trace(cfMatVid);
    accuracy = ((sum_diagonal)/(sum_total)) * 100;
    finalAnalysis{index,3} = accuracy;
    finalAnalysis{index,1} = strcat('Together',',',num2str(modelNo),',Video');
    finalAnalysis{index+1,2} = cfMatAud;
    finalAnalysis{index+1,1} = strcat('Together',',',num2str(modelNo),',Audio');
    sum_total = sum(sum(cfMatAud));
    sum_diagonal = trace(cfMatAud);
    accuracy = ((sum_diagonal)/(sum_total)) * 100;
    finalAnalysis{index+1,3} = accuracy;
    index = index + 2;
    
end
