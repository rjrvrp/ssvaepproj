
load('AllDataCompre.mat');

allFeatures = struct();

for methNo = 1:3
    
    method = methods{methNo};
    
    if(~isfield(allFeatures,method))
        allFeatures.(method) = struct();
    end

    methData = cell(1,11);
    for n = 1:11
        thatmethodData = analyData.(patie{n}).(method).(strcat(patie{n},nume{n},'data','_',num2str(sorw{n})));
%         allFeatures.(method).(strcat('T',num2str(n))) = thatmethodData;
        thatmethodData = cell2mat(thatmethodData(2:end,3:6));
        thatmethodData(end-1,:) = thatmethodData(end-1,:) == 7.5;
        thatmethodData(end,:) = thatmethodData(end,:) == 37;
        methData{n} = thatmethodData;
    end
    meth = cell2mat(methData);
    allFeatures.(method).('DATA') = reshape(meth,size(thatmethodData,1),size(thatmethodData,2),11);
    
end

clear meth methData methNo method n thatmethodData
