function confMatr = crossValfunc(xtr,ytr,xte,yte)

    global modelfuncs modelNo order
    fh = modelfuncs{modelNo};
    model = fh(xtr,ytr);
    confMatr = confusionmat(yte,predict(model,xte),'order',order);
    
end