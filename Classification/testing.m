function models = testing(models, testdata)

s = size(models);
n = s(1);
for i = 1:n
    models{i,3} = predict(models{i,2}, testdata);
    models{i,4} = resubLoss(models{i,2}); %loss
    models{i,5} = models{i,4} * models{i,2}.NumObservations; %no of errors
    models{i,6} = confusionmat(models{i,2}.Y,resubPredict(models{i,2})); %confusion matrix
    cvmodel = crossval(models{i,2},'kfold',5);
    models{i,7} = kfoldLoss(cvmodel); %crossvalidation error
end

end
